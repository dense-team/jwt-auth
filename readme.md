# JWT authorization for PHP application

Package encapsulates JWT handling such as creating, validating and obtaing tokens for PHP application.

## Instalation

Run following composer command.

```shell
composer require dense/jwt-auth
```

Example usage:
```php
use Firebase\JWT\JWT;
use Dense\Jwt\Auth\Sign;

// create jwt sign
$adapter = new JWT();
$sign = new Sign($adapter, 'ISSUER', 'SECRETKEY');

// create jwt token
$token = $sign->make($userId, [
    'aud' => $audience,
]);

// validate token
try {
    $claims = $sign->decode();

    if($claims->sub === $userId) {
        // Authorization successfull
    }
} catch (\Exception $e) {
    
}
```

## Configuration

Variables that needs to be configured are jwt secret key and issuer. Both inserted as parameters in constructor of Sign class.

Token returned from client side must be in header (according to RFC 7519). Name of the header is "Authorization" and its value is "Bearer TOKENHASH". 
