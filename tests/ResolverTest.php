<?php
/**
 * User: Maros Jasan
 * Date: 31.1.2018
 * Time: 17:32
 */

use PHPUnit\Framework\TestCase;
use Firebase\JWT\JWT;
use Dense\Jwt\Auth\Sign;
use Dense\Jwt\Auth\Resolver;

class ResolverTest extends TestCase
{
    const DEFAULT_KEY = 'bc92608f0811dd64d9790cd4cc29524d';

    const DEFAULT_SUB = 1;
    const DEFAULT_ISS = 'DENSE';
    const DEFAULT_AUD = 'COMPANY';

    private function getJwtSign()
    {
        $adapter = new JWT();
        $sign = new Sign($adapter, self::DEFAULT_ISS, self::DEFAULT_KEY);

        return $sign;
    }

    private function createToken()
    {
        $sign = $this->getJwtSign();

        $token = $sign->make(self::DEFAULT_SUB, [
            'aud' => self::DEFAULT_AUD,
        ]);
        $this->setAuthHeader($token);

        return $token;
    }

    private function setAuthHeader($token)
    {
        $_SERVER = [
            'HTTP_AUTHORIZATION' => Resolver::AUTH_HEADER_PREFIX . ' ' . $token,
        ];
    }

    private function setEmptyAuthHeader()
    {
        $_SERVER = [
            'HTTP_AUTHORIZATION' => '',
        ];
    }

    private function unsetAuthHeader()
    {
        unset($_SERVER['HTTP_AUTHORIZATION']);
    }

    public function testResolvingAuthHeader()
    {
        $token = $this->createToken();

        $resolvedToken = Resolver::resolveToken();

        $this->assertEquals($resolvedToken, $token);
    }

    public function testResolvingEmptyAuthHeader()
    {
        $this->setEmptyAuthHeader();

        $resolvedToken = Resolver::resolveToken();

        $this->assertEquals($resolvedToken, null);
    }

    public function testResolvingUnsetAuthHeader()
    {
        $this->unsetAuthHeader();

        $resolvedToken = Resolver::resolveToken();

        $this->assertEquals($resolvedToken, null);
    }

    public function testPublishingAuthHeader()
    {
        $token = $this->createToken();

        $publishedHeader = Resolver::publishAuthHeader();
        $expectedHeader = Resolver::AUTH_HEADER_PREFIX . ' ' . $token;

        $this->assertEquals($publishedHeader, $expectedHeader);
    }
}
