<?php
/**
 * User: Maros Jasan
 * Date: 20.11.2016
 * Time: 16:18
 */

use PHPUnit\Framework\TestCase;
use Firebase\JWT\JWT;
use Dense\Jwt\Auth\Sign;
use Dense\Jwt\Auth\Resolver;

class SignTest extends TestCase
{
    const DEFAULT_KEY = 'bc92608f0811dd64d9790cd4cc29524d';

    const DEFAULT_SUB = 1;
    const DEFAULT_ISS = 'DENSE';
    const DEFAULT_AUD = 'COMPANY';

    private function getJwtSign()
    {
        $adapter = new JWT();
        $sign = new Sign($adapter, self::DEFAULT_ISS, self::DEFAULT_KEY);

        return $sign;
    }

    private function createAndPublishToken()
    {
        $sign = $this->getJwtSign();

        $token = $sign->make(self::DEFAULT_SUB, [
            'aud' => self::DEFAULT_AUD,
        ]);
        $this->setAuthHeader($token);

        return $sign;
    }

    private function setAuthHeader($token)
    {
        $_SERVER = [
            'HTTP_AUTHORIZATION' => Resolver::AUTH_HEADER_PREFIX . ' ' . $token,
        ];
    }

    public function testDecodeSign()
    {
        $sign = $this->createAndPublishToken();
        $resolvedClaims = $sign->decode();

        $expectedAud = self::DEFAULT_AUD;
        $resolvedAud = $resolvedClaims->aud;

        $this->assertEquals($resolvedAud, $expectedAud);
    }

    public function testExtendSign()
    {
        $sign = $this->createAndPublishToken();

        sleep(1);

        $actualToken = Resolver::resolveToken();
        $extendedToken = $sign->extend();

        $this->assertNotEquals($actualToken, $extendedToken);
    }
}
