<?php
/**
 * User: Maros Jasan
 * Date: 20.4.2017
 * Time: 20:52
 */

namespace Dense\Jwt\Auth;

class Resolver
{
    const AUTH_HEADER_PREFIX = 'Bearer';

    const JWT_COOKIE_NAME = 'jwt_token';

    /**
     * @return string
     */
    public static function resolveToken()
    {
        $token = self::resolveTokenFromHeader();

        if (!$token) {
            $token = self::resolveTokenFromCookie();
        }

        return $token;
    }

    /**
     * @return string
     */
    static public function resolveTokenFromHeader()
    {
        $token = null;

        // check if authorization header is present in request
        if (isset($_SERVER['HTTP_AUTHORIZATION'])) {

            // obtain value from authorization header
            $authHeader = $_SERVER['HTTP_AUTHORIZATION'];

            if (strpos($authHeader, self::AUTH_HEADER_PREFIX) === 0) {
                $token = substr($authHeader, strlen(self::AUTH_HEADER_PREFIX));
            } else {
                $token = $authHeader;
            }
        }

        $token = trim($token);

        return $token;
    }

    /**
     * @return string
     */
    static public function resolveTokenFromCookie()
    {
        $token = null;

        // check if authorization header is present in request
        if (isset($_COOKIE[self::JWT_COOKIE_NAME])) {

            // obtain value from authorization header
            $token = $_COOKIE[self::JWT_COOKIE_NAME];
        }

        $token = trim($token);

        return $token;
    }

    /**
     * @param string $token
     * @return string|null
     */
    public static function createAuthHeader($token)
    {
        if ($token) {
            return self::AUTH_HEADER_PREFIX . ' ' . $token;
        }
    }

    /**
     * @return string|null
     */
    static public function publishAuthHeader()
    {
        $token = self::resolveToken();

        return self::createAuthHeader($token);
    }

    /**
     * @param string $type
     *
     * @return string|null
     */
    static public function setAuthHeader($type)
    {
        $token = null;

        switch (strtolower($type)) {
            case 'api':
                $token = self::resolveTokenFromHeader();

                break;

            case 'web':
                $token = self::resolveTokenFromCookie();

                break;
        }

        return self::createAuthHeader($token);
    }
}
