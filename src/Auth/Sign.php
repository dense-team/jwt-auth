<?php
/**
 * User: Maros Jasan
 * Date: 20.4.2017
 * Time: 20:46
 */

namespace Dense\Jwt\Auth;

use Firebase\JWT\JWT;

class Sign
{
    /**
     * @var \Firebase\JWT\JWT
     */
    protected $adapter;

    /**
     * @var string
     */
    protected $iss;

    /**
     * @var string
     */
    protected $secret;

    const TOKEN_EXPIRY = 7200;

    /**
     * Sign constructor.
     * @param \Firebase\JWT\JWT $adapter
     * @param string $iss
     * @param string $secret
     */
    public function __construct(JWT $adapter, $iss, $secret)
    {
        $this->adapter = $adapter;
        $this->iss = $iss;
        $this->secret = $secret;
    }

    /**
     * @param string $subject
     * @param array $params
     * @return string
     */
    public function make($subject, array $params = [])
    {
        $now = time();

        $data = [
            'sub' => $subject,
            'iss' => $this->iss,
            'aud' => null,
            'exp' => $now + self::TOKEN_EXPIRY,
            'iat' => $now,
            'nbf' => $now,
            'jti' => md5($subject . $now),
        ];

        unset($params['sub']);
        unset($params['iss']);
        unset($params['jti']);

        $data = $params + $data;

        // compatibility with PHP5
        $adapter = $this->adapter;

        $token = $adapter::encode($data, $this->secret);

        return $token;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function extend()
    {
        $claims = $this->decode();

        $token = $this->make($claims->sub, (array)$claims);

        return $token;
    }

    /**
     * @return \StdClass
     * @throws \Exception
     */
    public function decode()
    {
        $token = Resolver::resolveToken();

        if (!$token) {
            throw new \Exception('Empty authentication token.');
        }

        // compatibility with PHP5
        $adapter = $this->adapter;

        $claims = $adapter::decode($token, $this->secret, ['HS256']);

        return $claims;
    }

    /**
     * @return \StdClass
     * @throws \Exception
     */
    public function validate()
    {
        return $this->decode();
    }
}
